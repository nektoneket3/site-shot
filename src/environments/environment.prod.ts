const domain = 'http://siteshot-be.openode.io';

export const environment = {
  production: true,
  domain,
  apiBase: `${domain}/api`,
};
