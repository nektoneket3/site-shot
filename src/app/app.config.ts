import { environment } from '../environments/environment';


export default {
  domain: environment.domain,
  apiBase: environment.apiBase
};
