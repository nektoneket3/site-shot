import { Component } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';
import { computed } from 'mobx-angular';

import { AuthStore } from './shared/store/auth.store';
import { Throbber } from 'shared/modules/throbber';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  throbber = new Throbber();

  @computed get userSettings() {
    return this.authStore.user.settings;
  }

  constructor(
    private authStore: AuthStore,
    private router: Router
  ) {
    router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });
  }

  checkRouterEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.throbber.run();
    }

    if (
      routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError
    ) {
      this.throbber.stop();
    }
  }
}
