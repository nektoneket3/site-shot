import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import localStorage from 'mobx-localstorage';


@Component({
  selector: 'app-description-modal',
  templateUrl: './description-modal.component.html',
  styleUrls: ['./description-modal.component.scss']
})
export class DescriptionModalComponent implements OnInit {

  dontShowAgain: FormControl = new FormControl(false);

  private modalOptions: NgbModalOptions = {
    size: 'lg',
    centered: true
  };

  modalInstance: NgbModalRef;

  @ViewChild('modalContent')
  private modalContentRef: ElementRef;

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    if (localStorage.getItem('hideDescription')) {
      return;
    }

    setTimeout(async () => {
      this.modalInstance = this.modalService.open(this.modalContentRef, this.modalOptions);
      await this.modalInstance.result.catch(() => {});
      localStorage.setItem('hideDescription', this.dontShowAgain.value);
    });
  }

  closeModal(): void {
    this.modalInstance.close();
  }
}
