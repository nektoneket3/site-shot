import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import localStorage from 'mobx-localstorage';

import { SharedModule } from 'shared/shared.module';
import { DescriptionModalComponent } from './description-modal.component';


describe('DescriptionModalComponent', () => {
  let component: DescriptionModalComponent;
  let fixture: ComponentFixture<DescriptionModalComponent>;
  const modalId = '#description-modal-window';
  const clearStore = () => localStorage.delete('hideDescription');
  const needOpenModal = () => !localStorage.getItem('hideDescription');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DescriptionModalComponent],
      imports: [SharedModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeAll(clearStore);
  afterAll(clearStore);

  it('should render description modal, set dontShowAgain to true and close it', async () => {
    await fixture.whenStable();
    const modal = document.querySelector(modalId);

    if (needOpenModal) {
      expect(modal).toBeTruthy();
    }

    component.dontShowAgain.setValue(true);
    component.closeModal();
  });

  it('should not render description modal after dontShowAgain became false', async () => {
    await fixture.whenStable();
    const modal = document.querySelector(modalId);

    if (!needOpenModal) {
      expect(modal).toBeNull();
    }
  });
});
