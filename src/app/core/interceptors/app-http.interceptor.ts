import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable , throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { get } from 'lodash';
import { INTERNAL_SERVER_ERROR, UNAUTHORIZED, FORBIDDEN } from 'http-status-codes';
import localStorage from 'mobx-localstorage';

import { NotifierService } from 'shared/modules/notifier/services/notifier.service';
import { AuthService } from 'auth/services/auth.service';


@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

  constructor(
    private notifierService: NotifierService,
    private authService: AuthService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem('token');

    if (token) {
      request = request.clone({ headers: request.headers.set('Authorization', `JWT ${token}`)});
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if ((request as any).intercept) {

          if ([INTERNAL_SERVER_ERROR].includes(error.status)) {
            this.notifierService.error(error.statusText);
          }

          if ([UNAUTHORIZED, FORBIDDEN].includes(error.status)) {
            const message = get(error, 'error.errors[0].messages[0]', '');
            this.notifierService.error(
              `${error.statusText}${message ? ': ' : ''}${message}`
            );
            this.authService.logout();
          }
        }

        return throwError(error);
      }));
  }
}
