import { Injectable } from '@angular/core';
import localStorage from 'mobx-localstorage';

import { AuthStore } from 'shared/store/auth.store';
import { UsersResource } from 'users/services/users.resource';


@Injectable()
export class InitDataService {

  constructor(
    private authStore: AuthStore,
    private usersResource: UsersResource
  ) {}

  async initUser(): Promise<any> {
    if (localStorage.getItem('token')) {
      this.authStore.setUser(
        await this.usersResource.getCurrent()
      );
    }
  }
}
