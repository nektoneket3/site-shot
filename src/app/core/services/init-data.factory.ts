import { InitDataService } from './init-data.service';


export function setInitialData(initDataService: InitDataService): () => Promise<any> {
  return async () => {
    try {
      await Promise.all([
        initDataService.initUser()
      ]);
    } catch (error) {
      showError();
      throw error;
    }
  };
}
// TODO create service for it
function showError() {
  const initThrobber = document.getElementById('loader');
  const initError = document.getElementById('init-error');
  const text = document.createTextNode('Something went wrong');
  initError.appendChild(text);
  initThrobber.style.display = 'none';
  initError.style.display = 'block';
}
