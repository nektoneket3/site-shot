import { HttpResource } from 'shared/modules/resource';
import appConfig from 'app.config';


export class AppResource extends HttpResource {
  url = appConfig.apiBase;
}
