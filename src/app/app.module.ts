import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ResourceModule } from '@ngx-resource/handler-ngx-http';
// app modules
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { LayoutModule } from './layout/layout.module';
import { ProfileModule } from './profile/profile.module';
import { DashboardModule } from './dashboard/dashboard.module';
// app interceptors
import { AppHttpInterceptor } from './core/interceptors/app-http.interceptor';
// app components
import { AppComponent } from './app.component';
import { DescriptionModalComponent } from './core/components/description-modal/description-modal.component';
// app services
import { setInitialData } from './core/services/init-data.factory';
import { InitDataService } from './core/services/init-data.service';


@NgModule({
  declarations: [
    AppComponent,
    DescriptionModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ResourceModule.forRoot(), // TODO remove it
    AppRoutingModule,
    SharedModule,
    AuthModule,
    LayoutModule,
    ProfileModule,
    DashboardModule
  ],
  providers: [
    InitDataService,
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true },
    { provide: APP_INITIALIZER, useFactory: setInitialData, deps: [InitDataService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
