import { NgModule } from '@angular/core';
// modules
import { SharedModule } from '../shared/shared.module';
// services
import { UsersResource } from './services/users.resource';
import { UsersService } from './services/users.service';


@NgModule({
  declarations: [
  ],
  providers: [
    UsersResource,
    UsersService
  ],
  imports: [
    SharedModule,
  ],
  exports: []
})
export class UsersModule { }
