import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppResource } from 'core/services/app.resource';
import { GetMethod, SendMethod, Get, Patch } from 'shared/modules/resource';


@Injectable({ providedIn: 'root' })
export class UsersResource extends AppResource {

  path = '/users/current';

  @Get()
  getCurrent: GetMethod<any>;

  @Patch()
  update: SendMethod<any, any>;

  @Patch({
    path: '/avatar',
    bodyType: 'formData'
  })
  updateAvatar: SendMethod<any, any>;

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }
}
