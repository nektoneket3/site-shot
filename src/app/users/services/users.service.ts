import { Injectable } from '@angular/core';

import { AuthStore } from 'shared/store/auth.store';
import { UsersResource } from './users.resource';


@Injectable({ providedIn: 'root' })
export class UsersService {

  constructor(
    private authStore: AuthStore,
    private usersResource: UsersResource,
  ) {}

  async updateAvatar(user: { fullAvatar: File, minAvatar: File }): Promise<any> {
    const updatedUser = await this.usersResource.updateAvatar({
      fullAvatar: user.fullAvatar,
      minAvatar: [user.minAvatar, user.fullAvatar.name],
    });

    this.authStore.setUser(updatedUser);

    return updatedUser;
  }

  async update(userData) {
    const updatedUser = await this.usersResource.update({
      fullName: userData.fullName,
      settings: {
        backgroundColor: userData.backgroundColor,
        headerBackgroundColor: userData.headerBackgroundColor,
      }
    });
    this.authStore.setUser(updatedUser);
    return updatedUser;
  }
}

