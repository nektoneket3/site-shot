import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppResource } from 'core/services/app.resource';
import { SendMethod, GetMethod } from 'shared/modules/resource';
import { Site } from '../models/site.model';


type Url = string;

@Injectable()
export class SitesResource extends AppResource {

  path = '/sites';

  post: SendMethod<Url, Site>;
  get: GetMethod<Site[]>;

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }
}
