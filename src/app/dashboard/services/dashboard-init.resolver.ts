import { Injectable } from '@angular/core';

import { InitResolver } from 'shared/services/init.resolver';
import { SitesStore } from 'shared/store/sites.store';
import { SitesService } from './sites.service';


@Injectable()
export class DashboardInitResolver extends InitResolver {

  constructor(
    private sitesService: SitesService,
    sitesStore: SitesStore
  ) {
    super(sitesStore);
  }

  async init(): Promise<any> {
    super.init({
      recentSites: await this.sitesService.getSites()
    });
  }
}
