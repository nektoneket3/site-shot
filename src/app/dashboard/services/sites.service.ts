import { Injectable } from '@angular/core';

import { AuthStore } from 'shared/store/auth.store';
import { SitesResource } from './sites.resource';
import { SitesStore } from 'shared/store/sites.store';
import { Site } from '../models/site.model';


@Injectable({ providedIn: 'root' })
export class SitesService {
  constructor(
    private authStore: AuthStore,
    private sitesResource: SitesResource,
    private sitesStore: SitesStore
  ) {}

  async getSites(queryParams?: any): Promise<Site[]> {
    let sites = [];

    if (!this.authStore.user.isGuest) {
      sites = await this.sitesResource.get({ queryParams });
    }

    return sites;
  }

  async createSiteShot(url: string): Promise<any> {
    try {
      this.sitesStore.addSite(
        await this.sitesResource.post(url)
      );
    } catch ({ error: { errors } }) {
      this.sitesStore.setErrors(errors);
      this.sitesStore.switchCurrentSite();
    }
  }
}
