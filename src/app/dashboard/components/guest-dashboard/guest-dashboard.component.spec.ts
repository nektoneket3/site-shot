import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SharedModule } from 'shared/shared.module';
import { DashboardModule } from '../../dashboard.module';
import { SitesStore } from 'shared/store/sites.store';
import { sitesStoreStub } from 'shared/store/sites-stub.store';
import { GuestDashboardComponent } from './guest-dashboard.component';


describe('GuestDashboardComponent', () => {
  let component: GuestDashboardComponent;
  let fixture: ComponentFixture<GuestDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        DashboardModule,
      ],
      providers: [
        { provide: SitesStore, useValue: sitesStoreStub },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
