import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { AuthStore } from 'shared/store/auth.store';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {

  constructor(public authStore: AuthStore) {}

  ngOnInit() {}
}
