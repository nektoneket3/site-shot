import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { reaction } from 'mobx';
import { computed } from 'mobx-angular';

import { SitesService } from '../../services/sites.service';
import { Site } from '../../models/site.model';
import { SitesStore } from 'shared/store/sites.store';
import { Throbber } from 'shared/modules/throbber';


@Component({
  selector: 'app-screen-maker',
  templateUrl: './screen-maker.component.html',
  styleUrls: ['./screen-maker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScreenMakerComponent implements OnInit {

  urlInput = {
    defaultValue: 'https://example.com',
    errorMessage: 'Incorrect url'
  };

  modalOptions: NgbModalOptions = { size: 'lg', centered: true };
  siteForm: FormGroup;
  throbber = new Throbber();

  @computed get currentSite(): Site {
    return this.sitesStore.currentSite;
  }

  @computed get errors() {
    return this.sitesStore.errors;
  }

  get controls(): any {
    return this.siteForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private sitesService: SitesService,
    private sitesStore: SitesStore,
    private modalService: NgbModal,
  ) {

    this.siteForm = formBuilder.group({
      url: [this.urlInput.defaultValue, [
        Validators.required,
        Validators.pattern(
          'https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)'
        )
      ]],
    });

    reaction(() => this.currentSite, this.setUrlInput.bind(this));
  }

  @Throbber.on()
  submit(): Promise<any> {
    return this.sitesService.createSiteShot(this.siteForm.value);
  }

  openScreenShot(content): void {
    this.modalService.open(content, this.modalOptions);
  }

  private setUrlInput(currentSite: Site): void {
    if (currentSite) {
      this.siteForm.patchValue({ url: currentSite.url });
    }
  }

  ngOnInit() {}
}
