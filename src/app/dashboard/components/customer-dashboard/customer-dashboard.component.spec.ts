import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SharedModule } from 'shared/shared.module';
import { DashboardModule } from '../../dashboard.module';
import { SitesStore } from 'shared/store/sites.store';
import { sitesStoreStub } from 'shared/store/sites-stub.store';
import { CustomerDashboardComponent } from './customer-dashboard.component';


describe('CustomerDashboardComponent', () => {
  let component: CustomerDashboardComponent;
  let fixture: ComponentFixture<CustomerDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        DashboardModule,
      ],
      providers: [
        { provide: SitesStore, useValue: sitesStoreStub },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
