import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { computed, observable, action } from 'mobx-angular';
import { isNull, isEmpty } from 'lodash';

import { SitesStore } from 'shared/store/sites.store';
import { Throbber } from 'shared/modules/throbber';
import { SitesService } from '../../services/sites.service';
import { Site } from '../../models/site.model';


@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['../dashboard/dashboard.component.scss', './customer-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomerDashboardComponent implements OnInit {

  @observable sitesHistory: Site[] = null;

  @computed get recentUrls() {
    return this.sitesStore.recentSites;
  }

  @computed get noResults() {
    return !this.throbber.status && !isNull(this.sitesHistory) && isEmpty(this.sitesHistory);
  }

  throbber = new Throbber();

  noResIcon = '/assets/images/no-res.jpg';

  constructor(
    private sitesStore: SitesStore,
    private sitesService: SitesService,
  ) {}

  switchCurrentSite(siteIndex: number): void {
    this.sitesStore.switchCurrentSite(siteIndex);
  }

  @action @Throbber.on()
  async search(searchString: string): Promise<void> {
    if (searchString) {
      this.sitesHistory = await this.sitesService.getSites({ searchString });
    } else {
      this.sitesHistory = null;
    }
  }

  select(site: Site) {
    this.sitesStore.setSiteHistory(site);
  }

  ngOnInit() {}
}
