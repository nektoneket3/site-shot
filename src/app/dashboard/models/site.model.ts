import { getAbsolutePath } from 'shared/pipes/absolute-path.pipe';


interface Query {
  string?: string;
  dict?: any;
}

export class Site {
  screenShot: string;
  url: string;
  origin: string;
  protocol: string;
  host: string;
  hostName: string;
  path: string;
  pathName: string;
  query: Query;
  user: string;
  created: string;
  _id: string;

  constructor(site: Site) {
    this.screenShot = getAbsolutePath(site.screenShot);
    this.url = site.url || '';
    this.origin = site.origin || '';
    this.protocol = site.protocol || '';
    this.host = site.host || '';
    this.hostName = site.hostName || '';
    this.path = site.path || '';
    this.pathName = site.pathName || '';
    this.query = site.query || {};
    this.user = site.user || '';
    this.created = site.created || '';
    this._id = site._id || '';
  }
}
