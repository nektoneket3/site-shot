import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// components
import { DashboardComponent } from './components/dashboard/dashboard.component';
// services
import { DashboardInitResolver } from './services/dashboard-init.resolver';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    resolve: { init: DashboardInitResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DashboardRoutingModule { }
