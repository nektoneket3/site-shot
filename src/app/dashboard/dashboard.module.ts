import { NgModule } from '@angular/core';
// modules
import { SharedModule } from 'shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
// components
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GuestDashboardComponent } from './components/guest-dashboard/guest-dashboard.component';
import { CustomerDashboardComponent } from './components/customer-dashboard/customer-dashboard.component';
import { ScreenMakerComponent } from './components/screen-maker/screen-maker.component';
// services
import { SitesResource } from './services/sites.resource';
import { SitesService } from './services/sites.service';
import { DashboardInitResolver } from './services/dashboard-init.resolver';


@NgModule({
  declarations: [
    DashboardComponent,
    GuestDashboardComponent,
    CustomerDashboardComponent,
    ScreenMakerComponent
  ],
  providers: [
    SitesResource,
    SitesService,
    DashboardInitResolver
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
