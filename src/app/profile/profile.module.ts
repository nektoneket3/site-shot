import { NgModule } from '@angular/core';
// modules
import { SharedModule } from 'shared/shared.module';
import { ProfileRoutingModule } from './profile-routing.module';
// components
import { ProfileComponent } from './components/profile/profile.component';


@NgModule({
  declarations: [ProfileComponent],
  imports: [
    SharedModule,
    ProfileRoutingModule
  ]
})
export class ProfileModule { }
