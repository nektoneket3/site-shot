import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// components
import { ProfileComponent } from './components/profile/profile.component';
// services
import { RoleGuardService } from 'shared/services/role-guard.service';

import { guest } from 'auth/role';


const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [RoleGuardService],
    data: { notPermittedRoles: [guest] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ProfileRoutingModule { }
