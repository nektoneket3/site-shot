import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { computed } from 'mobx-angular';

import { AuthStore } from 'shared/store/auth.store';
import { UsersService } from 'users/services/users.service';
import { Throbber } from 'shared/modules/throbber';


interface FormControls {
  [key: string]: AbstractControl;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userForm: FormGroup;
  throbber = new Throbber();

  @computed get user() {
    return this.authStore.user;
  }

  get controls(): FormControls {
    return this.userForm.controls;
  }

  constructor(
    public authStore: AuthStore,
    private formBuilder: FormBuilder,
    private usersService: UsersService
  ) {
    this.userForm = formBuilder.group({
      fullName: [this.user.fullName],
      backgroundColor: [this.user.settings.backgroundColor],
      headerBackgroundColor: [this.user.settings.headerBackgroundColor],
    });
  }

  @Throbber.on({ delay: 300 })
  submit() {
    return this.usersService.update(this.userForm.value);
  }

  ngOnInit() {}
}
