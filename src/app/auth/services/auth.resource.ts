import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppResource } from 'core/services/app.resource';
import { SendMethod, Create, Post } from 'shared/modules/resource';


interface BaseCredentials {
  email: string;
  password: string;
}

interface SignupCredentials extends BaseCredentials {
  fullName?: string;
}

@Injectable()
export class AuthResource extends AppResource {

  path = '/auth';

  @Post({
    path: '/login',
    intercept: false
  })
  login: SendMethod<BaseCredentials, { user: any, token: string }>;

  @Create({
    path: '/signup'
  })
  signup: SendMethod<SignupCredentials, any>;

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }
}
