import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { AuthStore } from 'shared/store/auth.store';
import { AuthResource } from './auth.resource';


@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(
    private router: Router,
    private authStore: AuthStore,
    private authResource: AuthResource,
  ) {}

  async login(credentials): Promise<any> {
    let response;

    try {
      response = await this.authResource.login(credentials);
    } catch (errorResponse) {
      if (!(errorResponse instanceof HttpErrorResponse)) {
        throw errorResponse;
      }
      this.authStore.setErrors(errorResponse.error.errors);
    }

    if (response) {
      this.authStore.setUser(response.user);
      this.authStore.setToken(response.token);
      this.authStore.setErrors();
      this.router.navigate(['/']); // TODO
    }
  }

  async signup(credentials): Promise<any> {
    const user = await this.authResource.signup(credentials).catch(({ error: { errors } }) => {
      this.authStore.setErrors(errors);
    });
    if (user) {
      await this.login(credentials);
    }
  }

  logout() {
    this.authStore.setUser();
    this.authStore.setToken();
    this.router.navigate(['/login']); // TODO
  }
}

