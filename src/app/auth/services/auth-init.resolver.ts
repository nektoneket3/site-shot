import { Injectable } from '@angular/core';

import { AuthStore } from 'shared/store/auth.store';
import { InitResolver } from 'shared/services/init.resolver';


@Injectable()
export class AuthInitResolver extends InitResolver {

  constructor(private authStore: AuthStore) {
    super(authStore);
  }
}
