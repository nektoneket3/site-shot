import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { computed } from 'mobx-angular';

import { Throbber } from 'shared/modules/throbber';
import { AuthStore } from 'shared/store/auth.store';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {

  throbber = new Throbber();
  loginForm: FormGroup;

  @computed get errors() {
    return this.authStore.errors;
  }

  get controls(): any {
    return this.loginForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private authStore: AuthStore
  ) {
    this.loginForm = formBuilder.group({
      email: ['', [Validators.required, Validators.email]], // Validators.pattern('[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}')]],
      password: ['', [Validators.required]]
    });
  }

  @Throbber.on()
  submit() {
    return this.authService.login(this.loginForm.value);
  }

  @Throbber.on()
  testLogin(email) {
    return this.authService.login({
      email,
      password: email,
    });
  }

  ngOnInit() {}
}
