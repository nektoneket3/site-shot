import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { computed } from 'mobx-angular';

import { Throbber } from 'shared/modules/throbber';
import { AuthService } from '../../services/auth.service';
import { AuthStore } from 'shared/store/auth.store';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {

  throbber = new Throbber();
  signupForm: FormGroup;

  @computed get errors() {
    return this.authStore.errors;
  }

  get controls() {
    return this.signupForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private authStore: AuthStore
  ) {
    this.signupForm = formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
        fullName: ['']
    });
  }

  @Throbber.on()
  submit() {
    return this.authService.signup(this.signupForm.value);
  }

  ngOnInit() {}
}
