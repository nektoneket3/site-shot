import { NgModule } from '@angular/core';
// modules
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';
// components
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
// services
import { AuthResource } from './services/auth.resource';
import { AuthService } from './services/auth.service';
import { AuthInitResolver } from './services/auth-init.resolver';


@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent
  ],
  providers: [
    AuthResource,
    AuthService,
    AuthInitResolver
  ],
  imports: [
    AuthRoutingModule,
    SharedModule
  ]
})
export class AuthModule { }
