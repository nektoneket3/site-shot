import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// components
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
// services
import { RoleGuardService } from 'shared/services/role-guard.service';
import { AuthInitResolver } from './services/auth-init.resolver';

import { guest } from './role';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [RoleGuardService],
    data: { permittedRoles: [guest], redirectTo: '/' }, // TODO route string
    resolve: { init: AuthInitResolver }
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [RoleGuardService],
    data: { permittedRoles: [guest], redirectTo: '/' }, // TODO route string
    resolve: { init: AuthInitResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AuthRoutingModule { }
