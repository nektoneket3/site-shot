import { computed } from 'mobx-angular';

import { getAbsolutePath } from 'shared/pipes/absolute-path.pipe';
import { guest, customer, admin } from '../role';
import { OutputColor } from 'shared/modules/color-picker/models/output-color.model';


interface UserSettings {
  backgroundColor: OutputColor;
  headerBackgroundColor: OutputColor;
}

export class User {
  role?: string;
  fullName?: string;
  fullAvatar?: string;
  minAvatar?: string;
  email?: string;
  created?: string;
  settings?: UserSettings;
  _id?: string;

  constructor(user: any = {}) {
    this.role = user.role || guest;
    this.fullName = user.fullName || '';
    this.fullAvatar = getAbsolutePath(user.fullAvatar);
    this.minAvatar = getAbsolutePath(user.minAvatar);
    this.email = user.email || '';
    this.created = user.created || '';
    this.settings = user.settings || {};
    this._id = user._id || '';
  }

  @computed get isGuest(): boolean {
    return this.role === guest;
  }

  @computed get isCustomer(): boolean {
    return this.role === customer;
  }

  @computed get isAdmin(): boolean {
    return this.role === admin;
  }
}
