import { Component, OnInit, Input } from '@angular/core';
import { isUndefined } from 'lodash';

import { Throbber } from 'shared/modules/throbber';


@Component({
  selector: 'app-throbber-overlay',
  templateUrl: './throbber-overlay.component.html',
  styleUrls: ['./throbber-overlay.component.scss']
})
export class ThrobberOverlayComponent implements OnInit {

  @Input() isVisible?: boolean;
  @Input() throbber?: Throbber;
  @Input() inheritColorFrom?: string;

  get throbberStatus() {
    return !isUndefined(this.isVisible) ? this.isVisible : this.throbber.status;
  }

  get backgroundColor(): string {
    if (!this.inheritColorFrom) {
      return '';
    }
    let backgroundColor = (document.getElementsByClassName(this.inheritColorFrom)[0] as any).style.backgroundColor;
    if (backgroundColor) {
      backgroundColor = (backgroundColor || '').match(/\d+/g);
      backgroundColor[3] = '0.5';
      return `rgb(${backgroundColor.join(',')})`;
    }
  }

  ngOnInit() {}
}
