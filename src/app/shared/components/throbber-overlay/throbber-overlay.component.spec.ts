import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from 'shared/shared.module';
import { ThrobberOverlayComponent } from './throbber-overlay.component';
import { ProcessState } from 'shared/decorators/throbber.decorator';


describe('ThrobberOverlayComponent', () => {
  let component: ThrobberOverlayComponent;
  let fixture: ComponentFixture<ThrobberOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThrobberOverlayComponent);
    component = fixture.componentInstance;
    // input property
    component.throbber = new ProcessState();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
