import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { isString } from 'lodash';


interface DictItem {
  [displayKey: string]: string;
}

type StringItem = string;

interface DropdownItem {
  text: string;
  value: any;
}

type InputDropdownItem = DictItem | StringItem;

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {

  @Input() debounce = 500;
  @Input() displayKey: string;
  @Input('dropdownItems')
  set inputDropdownItems(dropdownItems: InputDropdownItem[]) {
    this.dropdownItems = this.getDropdownItems(dropdownItems);
  }
  @Output() search: EventEmitter<string> = new EventEmitter();
  @Output() itemClick: EventEmitter<any> = new EventEmitter();

  protected defaultDisplayKey = 'text';

  searchControl: FormControl = new FormControl('');
  dropdownItems: DropdownItem[] = [];
  isDropdownHidden = true;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    this.searchControl.valueChanges.pipe(
      debounceTime(this.debounce),
      distinctUntilChanged())
    .subscribe((searchString) => {
      this.search.emit(searchString);
    });
  }

  hideDropdown(): void {
    this.isDropdownHidden = true;
  }

  showDropdown(): void {
    this.isDropdownHidden = false;
  }

  itemSelect(value: any): void {
    this.itemClick.emit(value);
    this.hideDropdown();
  }

  @HostListener('document:click', ['$event'])
  clickOutside(event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.hideDropdown();
    }
  }

  private getDropdownItems(dropdownItems: InputDropdownItem[]): DropdownItem[] {
    return (dropdownItems || []).map((item: InputDropdownItem): DropdownItem => {
      if (isString(item)) {
        return {
          text: item as StringItem,
          get value() {
            return this.text;
          }
        };
      }

      const text = item[this.displayKey] || item[this.defaultDisplayKey];

      if (text) {
        return {
          text,
          value: item
        };
      }

      throw new Error('Passed dropdown items do not have "text" key. Add it or pass "displayKey" into component.');
    });
  }
}
