import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { isBoolean } from 'lodash';

import { Error } from 'shared/store/base.store';


@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  @Input() control: FormControl = null;
  @Input() errors?: Error[];
  @Input() className = 'form-control';
  @Input() type = 'text';
  @Input() name: string = null;
  @Input() label = true;
  @Input() placeholder?: string;
  @Input() value?: any;
  @Input('readonly')
  set inputReadonly(readonly: boolean) {
    if (readonly) {
      if (isBoolean(readonly)) {
        this.readonly = readonly;
      }
      if (this.control) {
        this.control.disable();
      }
    }
  }

  get inputControl() {
    return this.control || new FormControl(this.value);
  }

  readonly = false;

  private _message = 'Invalid';

  get message(): string {
    return this._message;
  }

  @Input() set message(message: string) {
    this._message = message || this._message;
  }

  ngOnInit() {}
}
