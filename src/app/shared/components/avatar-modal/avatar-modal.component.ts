import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import { ImageCroppedEvent } from 'ngx-image-cropper';
import { UsersService } from 'users/services/users.service';
import { Throbber } from 'shared/modules/throbber';


@Component({
  selector: 'app-avatar-modal',
  templateUrl: './avatar-modal.component.html',
  styleUrls: ['./avatar-modal.component.scss']
})
export class AvatarModalComponent implements OnInit {

  imageChangedEvent: any = '';
  croppedImageEvent: any = '';
  modalOptions: NgbModalOptions = { size: 'lg', centered: true };
  throbber = new Throbber();

  constructor(
    private modalService: NgbModal,
    private usersService: UsersService,
  ) {}

  async open(content) {
    try {
      await this.modalService.open(content, this.modalOptions).result;
    } catch (reason) {
      this.resetInput();
    }
  }

  @Throbber.on({ delay: 300 })
  async save(avatarModal) {
    try {
      await this.usersService.updateAvatar({
        fullAvatar: this.imageChangedEvent.target.files[0],
        minAvatar: this.croppedImageEvent.file
      });
      this.resetInput();
      avatarModal.close();
    } catch (error) {
      // TODO add error handling
      console.log(error);
    }
  }

  resetInput(): void {
    this.imageChangedEvent = '';
    this.croppedImageEvent = '';
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImageEvent = event;
  }

  loadImageFailed() {
    // show message
  }

  ngOnInit() {}
}
