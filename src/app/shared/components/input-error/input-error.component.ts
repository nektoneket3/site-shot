import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

import { fadeInOut } from 'shared/animations/fade-in-out.animation';
import { Error } from 'shared/store/base.store';


@Component({
  selector: 'app-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.scss'],
  animations: [fadeInOut()],
})
export class InputErrorComponent implements OnInit {

  @Input() control: FormControl;
  @Input() errors?: Error;
  private _value: string;

  get value() {
    return this._value;
  }

  @Input() set value(value) {
    if (this.errors) {
      this.errors.clear();
    }
    this._value = value;
  }

  ngOnInit() {}
}
