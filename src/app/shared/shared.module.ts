import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// vendor modules and components
import { MobxAngularModule } from 'mobx-angular';
import { NgbDropdownModule, NgbModalModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { ImageCropperModule } from 'ngx-image-cropper';
// modules
import { NotifierModule } from './modules/notifier/notifier.module';
import { ColorPickerModule } from './modules/color-picker/color-picker.module';
import { ThrobberModule } from './modules/throbber/throbber.module';
// services
import { AuthStore } from './store/auth.store';
import { SitesStore } from './store/sites.store';
import { RoleGuardService } from './services/role-guard.service';
// pipes
import { AbsolutePathPipe } from './pipes/absolute-path.pipe';
import { InvalidIfPipe } from './pipes/invalid-if.pipe';
// directives
import { BackgroundControlDirective } from './directives/background-control.directive';
// components
import { ThrobberOverlayComponent } from './components/throbber-overlay/throbber-overlay.component';
import { AvatarModalComponent } from './components/avatar-modal/avatar-modal.component';
import { InputErrorComponent } from './components/input-error/input-error.component';
import { InputComponent } from './components/input/input.component';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { ButtonComponent } from './components/button/button.component';


@NgModule({
  declarations: [
    AbsolutePathPipe,
    InvalidIfPipe,
    BackgroundControlDirective,
    AvatarModalComponent,
    ThrobberOverlayComponent,
    InputComponent,
    InputErrorComponent,
    SearchInputComponent,
    ButtonComponent,
  ],
  providers: [
    AuthStore,
    SitesStore,
    RoleGuardService,
  ],
  imports: [
    CommonModule,
    ImageCropperModule,
    MobxAngularModule,
    NgbDropdownModule,
    NgbPopoverModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ThrobberModule,
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MobxAngularModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbPopoverModule,
    ImageCropperModule,
    NotifierModule,
    ColorPickerModule,
    ThrobberModule,
    AbsolutePathPipe,
    BackgroundControlDirective,
    AvatarModalComponent,
    ThrobberOverlayComponent,
    InputComponent,
    InputErrorComponent,
    SearchInputComponent,
    ButtonComponent,
  ]
})
export class SharedModule { }
