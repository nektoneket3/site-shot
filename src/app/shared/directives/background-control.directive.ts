import { Directive, Input, ElementRef } from '@angular/core';
import { isString } from 'lodash';


@Directive({
  selector: '[appBackgroundControl]'
})
export class BackgroundControlDirective {

  constructor(private el: ElementRef) {}

  @Input() set
  color(color) {
    let _color = '';

    if (isString(color)) {
      _color = color || _color;
    } else {
      _color = color ? color.tone || color.color : _color;
    }

    this.el.nativeElement.style.backgroundColor = _color;
  }
}
