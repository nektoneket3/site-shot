import { ElementRef } from '@angular/core';

import { BackgroundControlDirective } from './background-control.directive';


describe('BackgroundControlDirective', () => {
  it('should create an instance', () => {
    const directive = new BackgroundControlDirective(new ElementRef(null));
    expect(directive).toBeTruthy();
  });
});
