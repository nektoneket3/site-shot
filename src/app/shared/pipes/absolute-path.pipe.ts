import { Pipe, PipeTransform } from '@angular/core';

import appConfig from 'app.config';


@Pipe({ name: 'absolutePath' })
export class AbsolutePathPipe implements PipeTransform {
  transform(relativePath: string): string {
    return getAbsolutePath(relativePath);
  }
}

export function getAbsolutePath(relativePath: string = ''): string {
  if (
    !relativePath ||
    relativePath.startsWith(appConfig.domain) ||
    relativePath.startsWith('data:image/png;base64')
  ) {
    return relativePath;
  }

  return `${appConfig.domain}${relativePath.startsWith('/') ? relativePath : `/${relativePath}`}`;
}
