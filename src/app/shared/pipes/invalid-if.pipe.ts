import { Pipe, PipeTransform } from '@angular/core';
import { Subject, timer } from 'rxjs';
import { debounce, distinctUntilChanged } from 'rxjs/operators';


@Pipe({ name: 'invalidIf' })
export class InvalidIfPipe implements PipeTransform {

  isValid = new Subject<[boolean, number]>();
  validationResult = new Subject<boolean>();

  constructor() {
    this.isValid.pipe(
      debounce(([, time]) => timer(time)),
      distinctUntilChanged()
    ).subscribe(([isValid]) => {
      this.validationResult.next(isValid);
    });
  }

  transform(value: string, isValid: boolean, debounceTime = 400): any {
    this.isValid.next([isValid, debounceTime]);
    return this.validationResult;
  }
}
