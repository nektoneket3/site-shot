import { SitesStore } from './sites.store';


export const sitesStoreStub: Partial<SitesStore> = {
  recentSites: [],
  errors: {}
};
