import { Injectable } from '@angular/core';
import { observable, action, computed } from 'mobx-angular';
import { reaction } from 'mobx';
import { isNull } from 'lodash';

import { Site } from 'dashboard/models/site.model';
import { BaseStore, Error } from './base.store';
import { NotifierService } from 'shared/modules/notifier/services/notifier.service';


@Injectable({ providedIn: 'root' })
export class SitesStore extends BaseStore {

  private recentSitesCount = 5;

  @observable sites: Site[];
  @observable currentSiteIndex: number;
  @observable historySite: Site;

  @computed get recentSites(): Site[] {
    return this.sites.slice(0, this.recentSitesCount);
  }

  @computed get currentSite(): Site {
    return this.historySite || this.sites[this.currentSiteIndex];
  }

  constructor(private notifierService: NotifierService) {
    super();

    reaction(
      () => this.errors.screenShot,
      this.showError.bind(this)
    );

    reaction(
      () => !isNull(this.currentSiteIndex),
      needClearHistory => needClearHistory && this.setSiteHistory()
    );

    reaction(
      () => !!this.historySite,
      needSwitchCurrent => needSwitchCurrent && this.switchCurrentSite()
    );
  }

  @action init({ recentSites }: { recentSites: Site[] }): void {
    this.sites = recentSites.map(site => new Site(site));
    this.switchCurrentSite();
    this.setSiteHistory();
  }

  @action setSiteHistory(site: Site = null): void {
    this.historySite = site ? new Site(site) : site;
  }

  @action addSite(site: Site): void {
    this.sites = [new Site(site), ...this.recentSites.slice(0, this.recentSitesCount - 1)];
    this.switchCurrentSite(0);
  }

  @action switchCurrentSite(siteIndex: number = null): void {
    this.currentSiteIndex = siteIndex;
  }

  private showError(error: Error): void {
    if (error) {
      this.notifierService.error(error.errorText);
    }
  }
}
