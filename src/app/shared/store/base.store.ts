import { observable, action } from 'mobx-angular';
import { reduce } from 'lodash';


export interface DefaultState {
  errors: any;

  setErrors(errors?: any): void;
  init(initData?: any): void;
}

export class Error {
  messages: string[] = [];
  meta: any = {};

  constructor({ messages, ...meta }) {
    this.messages = messages;
    this.meta = meta;
  }

  get errorText(): string {
    return this.messages.join('\n');
  }

  clear(): void {
    this.messages = [];
    this.meta = {};
  }
}

export class BaseStore implements DefaultState {

  @observable errors: { [errorName: string]: Error } = {};

  @action setErrors(errors?: any): void {
    this.errors = !errors
      ? {}
      : {
        ...this.errors,
        ...reduce(errors, (acc, error) => {
          return { ...acc, ...(error.field ? { [error.field]: new Error(error) } : {}) };
        }, {})
      };
  }

  @action init(initData?: any): void {}
}
