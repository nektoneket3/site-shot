import { Injectable } from '@angular/core';
import { observable, action } from 'mobx-angular';
import localStorage from 'mobx-localstorage';

import { BaseStore } from './base.store';
import { User } from 'auth/models/user.model';


@Injectable({ providedIn: 'root' })
export class AuthStore extends BaseStore {

  @observable user: User = new User();

  @action setUser(user?: User): void {
    this.user = new User(user);
  }

  @action setToken(token = ''): void {
    localStorage.setItem('token', token);
  }
}
