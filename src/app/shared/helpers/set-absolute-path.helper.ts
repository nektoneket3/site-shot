import { get, set, has } from 'lodash';

import appConfig from 'app.config';


export function setAbsolutePath(fields: string[]): (data: any) => any {
  return (data) => {
    fields.forEach((field) => {

      if (has(data, field)) {
        const relativePath = get(data, field);

        if (relativePath && !relativePath.startsWith('data:image/png;base64')) {
          set(data, field, `${appConfig.domain}${relativePath.startsWith('/') ? relativePath : `/${relativePath}`}`);
        }
      }
    });

    return data;
  };
}
