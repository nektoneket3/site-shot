type ValueAndError = [any, any];

export function getValueOrThrow(valueAndError: ValueAndError, errorText: string): any {
  const [error, value] = valueAndError;
  if (error) {
    throw new Error(errorText);
  }
  return value;
}
