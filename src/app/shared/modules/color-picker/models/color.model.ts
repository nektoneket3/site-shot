import { observable, computed, action } from 'mobx-angular';


export type Channel = number;
export type Rgb = [Channel, Channel, Channel];

export class Color {

  tonesVisible = false;
  @observable rgb: Rgb;
  @observable tones?: Color[][];

  @computed get rgbString(): string {
    return `rgb(${this.rgb.join(',')})`;
  }

  @computed get tonesCashed(): boolean {
    return !!this.tones;
  }

  constructor(r, g, b) {
    this.rgb = [r, g, b];
  }

  @action setTones(tones: Color[][]): void {
    this.tones = tones;
  }

  toggleTones(): void {
    this.tonesVisible = !this.tonesVisible;
  }
}
