export interface OutputColor {
  color: string;
  tone: string;
}
