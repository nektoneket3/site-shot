import { Injectable } from '@angular/core';
import { range, uniqWith, isEqual, isEmpty, last } from 'lodash';

import { Color, Rgb, Channel } from '../models/color.model';


@Injectable({ providedIn: 'root' })
export class ColorsStore {

  achromaticColors: Color[] = [
    new Color(0,   0,   0  ), // black
    new Color(255, 255, 255), // white
  ];

  chromaticColors: Color[] = [
    new Color(255, 0,   0  ), // red
    new Color(255, 165, 0  ), // orange
    new Color(255, 255, 0  ), // yellow
    new Color(0,   255, 0  ), // green
    new Color(0,   255, 255), // blue
    new Color(0,   0,   255), // darkBlue
    new Color(255, 0,   255), // purple
  ];

  getChromaticTransition(transitionSize): Color[] {
    return this.getTransitionByChain(this.chromaticColors, transitionSize);
  }

  getAchromaticTransition(transitionSize): Color[] {
    return this.getTransitionByChain(this.achromaticColors, transitionSize);
  }

  getAchromaticTones(color: Color, transitionSize): Color[][] {
    return [
      this.getTransition(this.achromaticColors[0], color, transitionSize),
      this.getTransition(color, this.achromaticColors[1], transitionSize)
    ].map<Color[]>(colors => colors.slice(1, colors.length - 1));
  }

  getTransitionByChain(chainColors: Color[], transitionSize): Color[] {
    return uniqWith(chainColors.reduce(
      (colors: Color[], color: Color, index: number, src: Color[]): Color[] => {
        const nextColor: Color = src[++index];
        if (!nextColor) {
          return colors;
        }
        return [...colors, ...this.getTransition(color, nextColor, transitionSize)];
      },
      []
    ), isEqual);
  }

  getTransition(from: Color, to: Color, transitionSize: number): Color[] {
    // find which r/g/b channels should be transformed
    const diffIndexes: number[] = from.rgb.reduce(
      (_diffIndexes, value, index): number[] => {
        if (value !== to[index]) {
          _diffIndexes.push(index);
        }
        return _diffIndexes;
      },
      []
    );
    return [
      ...range(transitionSize).reduce(
        (transitioned: Color[], ): Color[] => {
          const nextRgb: Rgb = diffIndexes.reduce((_nextRgb: Rgb, diffIndex: number): Rgb => {
            const fromDiffChanel: Channel = from.rgb[diffIndex];
            const toDiffChanel: Channel = to.rgb[diffIndex];
            const step: number = Math.round(Math.abs(fromDiffChanel - toDiffChanel) / (transitionSize + 1));

            fromDiffChanel < toDiffChanel ? _nextRgb[diffIndex] += step : _nextRgb[diffIndex] -= step;

            return _nextRgb;
          }, last(transitioned).rgb.slice(0));

          return [...transitioned, new Color(...nextRgb)];
        },
        [new Color(...from.rgb)]
      ),
      new Color(...to.rgb)
    ];
  }
}
