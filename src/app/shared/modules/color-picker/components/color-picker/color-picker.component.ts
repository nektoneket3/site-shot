import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ColorsStore } from '../../store/colors.store';
import { Color } from '../../models/color.model';
import { OutputColor } from '../../models/output-color.model';


@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent implements OnInit {

  @Input() colorSteps = 2;
  @Input() achromaticColorSteps = 5;
  @Input() colorTones = 4;
  @Input() selected: OutputColor;
  @Output() select: EventEmitter<OutputColor> = new EventEmitter();

  chromaticTransition: Color[];
  achromaticTransition: Color[];

  constructor(private colorsStore: ColorsStore) {}

  ngOnInit() {
    this.chromaticTransition = this.colorsStore.getChromaticTransition(this.colorSteps);
    this.achromaticTransition = this.colorsStore.getAchromaticTransition(this.achromaticColorSteps);
  }

  onSelect(outputColor: OutputColor): void {
    this.select.emit(outputColor);
  }
}
