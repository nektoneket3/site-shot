import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

import { Color } from '../../models/color.model';
import { OutputColor } from '../../models/output-color.model';
import { ColorsStore } from '../../store/colors.store';


@Component({
  selector: 'app-color-tone-pick',
  templateUrl: './color-tone-pick.component.html',
  styleUrls: ['./color-tone-pick.component.scss']
})
export class ColorTonePickComponent implements OnInit {

  @Input()
  set selected(selected: OutputColor) {
    if (selected) {
      this.selectedColor = selected;
    }
  }
  @Input() color: Color;
  @Input() tonesSize: number;
  @Output() select: EventEmitter<OutputColor> = new EventEmitter();

  selectedColor: OutputColor = { color: null, tone: null };

  constructor(private colorsStore: ColorsStore) {}

  @HostListener('mouseenter')
  mouseOverColor(): void {
    if (this.tonesSize) {
      // set hovered color's tones to achromatic transition
      if (!this.color.tonesCashed) {
        this.color.setTones(this.colorsStore.getAchromaticTones(this.color, this.tonesSize));
      }
      // show tones
      this.color.toggleTones();
    }
  }

  @HostListener('mouseleave')
  mouseLeaveColor(): void {
    if (this.tonesSize) {
      // hide tones
      this.color.toggleTones();
    }
  }

  onSelect(mainColor: Color, toneColor?: Color): void {
    this.select.emit({
      color: mainColor.rgbString,
      tone: toneColor ? toneColor.rgbString : null
    });
  }

  ngOnInit() {}
}
