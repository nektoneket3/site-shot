import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorPickerModule } from '../../color-picker.module';
import { ColorTonePickComponent } from './color-tone-pick.component';
import { Color } from '../../models/color.model';


describe('ColorTonePickComponent', () => {
  let component: ColorTonePickComponent;
  let fixture: ComponentFixture<ColorTonePickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ColorPickerModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorTonePickComponent);
    component = fixture.componentInstance;
    component.color = new Color(0, 0, 0);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
