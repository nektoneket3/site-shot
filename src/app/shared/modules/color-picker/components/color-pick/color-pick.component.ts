import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-color-pick',
  templateUrl: './color-pick.component.html',
  styleUrls: ['./color-pick.component.scss']
})
export class ColorPickComponent implements OnInit {

  @Input() selected: string;
  @Input() rgb: string;
  @Output() select: EventEmitter<void> = new EventEmitter();

  ngOnInit() {}
}
