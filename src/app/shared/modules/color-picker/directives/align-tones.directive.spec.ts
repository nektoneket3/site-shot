import { ElementRef } from '@angular/core';

import { AlignTonesDirective } from './align-tones.directive';


describe('AlignTonesDirective', () => {
  it('should create an instance', () => {
    const directive = new AlignTonesDirective(new ElementRef(null));
    expect(directive).toBeTruthy();
  });
});
