import { Directive, ElementRef, AfterViewInit } from '@angular/core';


@Directive({
  selector: '[appAlignTones]'
})
export class AlignTonesDirective implements AfterViewInit {

  constructor(private el: ElementRef) {}

  ngAfterViewInit() {
    const elWidth = this.el.nativeElement.getElementsByClassName('tones-part')[0].offsetWidth;
    const parentWidth = this.el.nativeElement.parentElement.offsetWidth;
    this.el.nativeElement.style.left = `${(parentWidth - elWidth) / 2}px`;
  }
}
