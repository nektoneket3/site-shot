import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// store
import { ColorsStore } from './store/colors.store';
// directives
import { AlignTonesDirective } from './directives/align-tones.directive';
// components
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { ColorPickComponent } from './components/color-pick/color-pick.component';
import { ColorTonePickComponent } from './components/color-tone-pick/color-tone-pick.component';


@NgModule({
  declarations: [
    AlignTonesDirective,
    ColorPickerComponent,
    ColorTonePickComponent,
    ColorPickComponent,
  ],
  providers: [
    ColorsStore,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ColorPickerComponent,
    ColorTonePickComponent,
  ]
})
export class ColorPickerModule { }
