import { Observable } from 'rxjs';
import { HttpEvent } from '@angular/common/http';


export type GET = 'GET';
export type POST = 'POST';
export type PUT = 'PUT';
export type PATCH = 'PATCH';
export type DELETE = 'DELETE';
export type OPTIONS = 'OPTIONS';
export type HEAD = 'HEAD';
export type TRACE = 'TRACE';
export type CONNECT = 'CONNECT';

export interface HttpMethods {
  get: GET;
  post: POST;
  put: PUT;
  patch: PATCH;
  delete: DELETE;
  options: OPTIONS;
  head: HEAD;
  trace: TRACE;
  connect: CONNECT;
}

export type RequestBodyType = 'json' | 'formData';
export type RequestMethod = GET | POST | PUT | PATCH | DELETE | OPTIONS | HEAD | TRACE | CONNECT;
export type ObservableRequestResponse<RR> = Observable<HttpEvent<RR>>;
export type RequestResponse<RR> = Promise<RR> | ObservableRequestResponse<RR>;

export interface RequestBody {
  [name: string]: any;
}

export interface RequestParams {
  [key: string]: string | number;
}

export interface RequestHeaders {
  [name: string]: string | string[];
}

export interface ResourceConfig {
  intercept?: boolean;
  toPromise?: boolean;
}
export interface DefaultResourceConfig {
  readonly intercept?: boolean;
  readonly toPromise?: boolean;
}

export interface RequestOptions extends ResourceConfig {
  headers?: RequestHeaders;
  reportProgress?: boolean;
  responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
  withCredentials?: boolean;
  path?: string;
  pathParams?: RequestParams;
  queryParams?: RequestHeaders;
  method?: GET | POST | PUT | PATCH | DELETE | OPTIONS | HEAD | TRACE | CONNECT;
  bodyType?: RequestBodyType;
}

export interface SendMethod<RB, RR> {
  // tslint:disable-next-line:callable-types
  (requestBody: RB, requestOptions?: RequestOptions): RR;
}

export interface GetMethod<RR> {
  // tslint:disable-next-line:callable-types
  (requestOptions?: RequestOptions): RR;
}

// type RequireAtLeastOne<T, Keys extends keyof T = keyof T> = TODO
//   Pick<T, Exclude<keyof T, Keys>>
//   & {
//       [K in Keys]-?: Required<Pick<T, K>> & Partial<Pick<T, Exclude<Keys, K>>>
//   }[Keys];
// export type ClickOrComponent = RequireAtLeastOne<Params, 'path' | 'query'>;

// type AtLeastOne<T, U = {[K in keyof T]: Pick<T, K> }> = Partial<T> & U[keyof U];
// type AtLeastOpeParams = AtLeastOne<Params, {[K in keyof Params]: Pick<Params, K>}>;
