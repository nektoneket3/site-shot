import { RequestBody, RequestOptions, RequestMethod } from '../declarations';
import { isSendMethod } from '../http-methods';


type Request = [RequestBody, RequestOptions];

export function parseRequestArgs(method: RequestMethod, args: IArguments | Request): Request {
  let body: RequestBody;
  let requestOptions: RequestOptions = args[0] || {};

  if (isSendMethod(method)) {
    body = args[0];
    requestOptions = args[1] || {};
  }

  return [body, requestOptions];
}
