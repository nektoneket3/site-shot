import { RequestParams } from '../declarations';


type ErroredParams = string[];
type BuiltUrl = string;

export function buildUrl(
  url: string,
  pathParams: RequestParams,
  paramsRegex = /{[(a-z)]\w+}/g
): [ErroredParams, BuiltUrl] {

  const paramsFromUrl: string[] = url.match(paramsRegex);

  if (!paramsFromUrl) {
    return [null, url];
  }

  const erroredParams: ErroredParams = paramsFromUrl.reduce((acc: ErroredParams, param: string): ErroredParams => {
    param = param.replace(/[{}]/g, '');
    const paramValue: string | number = pathParams[param];

    if (paramValue) {
      url = url.replace(paramsRegex, `${paramValue}`);
    } else {
      acc.push(param);
    }

    return acc;
  }, []);

  if (erroredParams.length) {
    return [erroredParams, null];
  }

  return [null, url];
}
