export function toFormData(body) {
  if (body) {
    return Object.entries(body).reduce((form, [key, value]: [string, string | string[]]) => {
      if (Array.isArray(value)) {
        form.append(key, value[0], value[1]);
      } else {
        form.append(key, value);
      }
      return form;
    }, new FormData());
  }
  return body;
}
