import { DefaultResourceConfig } from './declarations';


export const resourceConfig: DefaultResourceConfig = {
  intercept: true,
  toPromise: true,
};
