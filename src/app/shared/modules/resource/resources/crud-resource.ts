import { Resource } from './resource';
import { GetMethod, SendMethod } from '../declarations';
import { Crud } from '../decorators';


export abstract class CrudResource extends Resource {
  @Crud()
  create: SendMethod<any, any>;

  @Crud()
  retrieve: GetMethod<any>;

  @Crud()
  update: SendMethod<any, any>;

  @Crud()
  edit: SendMethod<any, any>;

  @Crud()
  destroy: GetMethod<any>;
}
