import { HttpResponse, HttpClient } from '@angular/common/http';

import { RequestOptions, ResourceConfig, RequestResponse } from '../declarations';
import { RequestMethod, RequestParams, ObservableRequestResponse } from '../declarations';
import { ResourceRequest } from '../models/resource-request';
import { buildUrl } from '../helpers';
import { resourceConfig } from '../resource.config';


export abstract class Resource {
  abstract url;
  path = '/';
  config: ResourceConfig = {};

  constructor(public httpClient: HttpClient) {}

  request<RB, RR>(
    method: RequestMethod,
    body?: RB,
    options: RequestOptions = {}
  ): RequestResponse<RR> {

    const request = new ResourceRequest(
      method,
      this.getUrl(options.path, options.pathParams),
      body,
      this.getRequestOptions(options)
    );

    return this.handleResponse<RR>(
      this.httpClient.request(request),
      options
    );
  }

  private handleResponse<RR>(
    response: ObservableRequestResponse<RR>,
    requestOptions: RequestOptions
  ): RequestResponse<RR> {

    if (this.getRequestOptions<boolean>(requestOptions, 'toPromise')) {
      return response.toPromise()
        .then((httpResponse: HttpResponse<RR>) => httpResponse.body);
    }

    return response;
  }

  private getUrl(
    path: string = '',
    pathParams: RequestParams = {}
  ): string {

    if (!this.url) {
      throw new Error('No URL provided');
    }

    const [erroredParams, url] = buildUrl(`${this.url}${this.path}${path}`, pathParams);

    if (erroredParams) {
      throw new Error(`Params: ${erroredParams.join(', ')} have not been passed`);
    }
    return url;
  }

  private getRequestOptions<T>(requestOptions: RequestOptions, option?: string): RequestOptions | T {
    const finalOptions: RequestOptions = {
      ...resourceConfig,
      ...this.config,
      ...requestOptions
    };

    return option ? finalOptions[option] : finalOptions;
  }
}
