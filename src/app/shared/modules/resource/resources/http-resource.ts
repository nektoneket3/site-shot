import { Resource } from './resource';
import { GetMethod, SendMethod } from '../declarations';
import { Get, Post, Put, Patch, Delete } from '../decorators';


export abstract class HttpResource extends Resource {
  @Get()
  get: GetMethod<any>;

  @Post()
  post: SendMethod<any, any>;

  @Put()
  put: SendMethod<any, any>;

  @Patch()
  patch: SendMethod<any, any>;

  @Delete()
  delete: GetMethod<any>;
}
