import { HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';

import { RequestOptions } from '../declarations';
import { toFormData } from '../helpers';


export class ResourceRequest extends HttpRequest<any> {
  intercept: boolean;

  constructor(method, url, body, requestOptions: RequestOptions) {

    if (requestOptions.bodyType === 'formData') {
      body = toFormData(body);
    }

    super(method, url, body, {
      headers: requestOptions.headers ? new HttpHeaders(requestOptions.headers) : undefined,
      params: requestOptions.queryParams
        ? new HttpParams({ fromObject: requestOptions.queryParams })
        : undefined,
      reportProgress: requestOptions.reportProgress,
      responseType: requestOptions.responseType,
      withCredentials: requestOptions.withCredentials
    });

    this.intercept = requestOptions.intercept;
  }
}
