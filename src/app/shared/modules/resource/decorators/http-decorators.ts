import { httpMethods } from '../http-methods';
import { getMethod } from './get-method';


export const Post = getMethod(httpMethods.post);
export const Get = getMethod(httpMethods.get);
export const Patch = getMethod(httpMethods.patch);
export const Put = getMethod(httpMethods.put);
export const Delete = getMethod(httpMethods.delete);
