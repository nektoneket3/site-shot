import { RequestOptions } from '../declarations';
import { Resource } from '../resources';
import { createMethod } from './create-method';


export function getMethod(method) {
  return (options?: RequestOptions) => {
    return (resource: Resource, methodName: string): void => {
      resource[methodName] = createMethod(method, options);
    };
  };
}
