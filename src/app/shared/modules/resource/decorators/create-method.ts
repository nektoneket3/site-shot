import { RequestOptions, RequestMethod } from '../declarations';
import { parseRequestArgs } from '../helpers';


export function createMethod(method: RequestMethod, actionOptions: RequestOptions = {}) {
  return function() {
    const [body, requestOptions] = parseRequestArgs(method, arguments);

    const finalRequestOptions: RequestOptions = {
      ...actionOptions,
      ...requestOptions,
    };

    return this.request(method, body, finalRequestOptions);
  };
}
