import { httpMethods } from '../http-methods';
import { getMethod } from './get-method';
import { createMethod } from './create-method';
import { RequestOptions } from '../declarations';
import { Resource } from '../resources';


const crudMethods = {
  create: 'create',
  retrieve: 'retrieve',
  update: 'update',
  edit: 'edit',
  destroy: 'destroy'
};

const crudToHttp = {
  [crudMethods.create]: httpMethods.post,
  [crudMethods.retrieve]: httpMethods.get,
  [crudMethods.update]: httpMethods.put,
  [crudMethods.edit]: httpMethods.patch,
  [crudMethods.destroy]: httpMethods.delete,
};

export const Create = getMethod(crudToHttp[crudMethods.create]);
export const Retreive = getMethod(crudToHttp[crudMethods.retrieve]);
export const Update = getMethod(crudToHttp[crudMethods.update]);
export const Edit = getMethod(crudToHttp[crudMethods.edit]);
export const Destroy = getMethod(crudToHttp[crudMethods.destroy]);

export function Crud(options?: RequestOptions) {
  return (resource: Resource, crudMethod: string): void => {
    const crudMethodsList = Object.values(crudMethods);

    if (!crudMethodsList.includes(crudMethod)) {
      throw new Error(`Use one of ${crudMethodsList.join(', ')}`);
    }

    resource[crudMethod] = createMethod(crudToHttp[crudMethod], options);
  };
}
