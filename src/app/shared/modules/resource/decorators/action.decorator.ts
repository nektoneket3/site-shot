import { RequestOptions } from '../declarations';
import { Resource } from '../resources';
import { createMethod } from './create-method';


export function Action(options?: RequestOptions) {
  return (resource: Resource, methodName: string): void => {
    resource[methodName] = createMethod(options.method, options);
  };
}
