import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobxAngularModule } from 'mobx-angular';
// components
import { NotifierComponent } from './components/notifier/notifier.component';
// services
import { NotificationsStore } from './store/notifications.store';
import { NotifierService } from './services/notifier.service';


@NgModule({
  declarations: [
    NotifierComponent
  ],
  providers: [
    NotificationsStore,
    NotifierService
  ],
  imports: [
    CommonModule,
    MobxAngularModule
  ],
  exports: [
    NotifierComponent
  ]
})
export class NotifierModule { }
