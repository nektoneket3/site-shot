import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { computed } from 'mobx-angular';

import { NotificationsStore } from '../../store/notifications.store';


@Component({
  selector: 'app-notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotifierComponent implements OnInit {

  @computed get notifications() {
    return this.notificationsStore.notifications;
  }

  constructor(
    private notificationsStore: NotificationsStore
  ) {}

  ngOnInit() {
  }
}
