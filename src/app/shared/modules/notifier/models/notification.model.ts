import { observable, action } from 'mobx-angular';
import { has } from 'lodash';


export class Notification {
  @observable isActive = true;
  @observable isVisible = true;
  message: string;
  color: string;
  id: number;

  private hideTimeout = 3000;
  private hideTimerId: number;
  private deactivateTimerId: number;

  get deactivateTimeout(): number {
    return this.hideTimeout + 1000;
  }

  constructor(notification: any = {}) {
    this.message = notification.message;
    this.color = notification.color || 'green';
    this.id = Date.now();
    this.setTimer();
  }

  @action setTimer(): void {
    this.hideTimerId = window.setTimeout(this.hide.bind(this), this.hideTimeout);
    this.deactivateTimerId = window.setTimeout(this.deactivate.bind(this), this.deactivateTimeout);
  }

  @action resetTimer(): void {
    this.hideTimeout = 1000;
    clearTimeout(this.hideTimerId);
    clearTimeout(this.deactivateTimerId);
    this.show();
  }

  @action show(): void {
    this.isVisible = true;
    this.isActive = true;
  }

  @action hide(): void {
    this.isVisible = false;
  }

  @action deactivate(): void {
    this.isActive = false;
  }
}
