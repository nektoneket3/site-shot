import { Injectable } from '@angular/core';
import { reaction } from 'mobx';
import { observable, action, computed } from 'mobx-angular';
import { findLastIndex } from 'lodash';

import { Notification } from '../models/notification.model';


@Injectable({ providedIn: 'root' })
export class NotificationsStore {

  @observable notifications: Notification[] = [];

  @computed get needClear(): boolean {
    return !this.notifications.filter(notification => notification.isActive).length;
  }

  constructor() {
    reaction(() => this.needClear, this.clearNotifications.bind(this));
  }

  @action add(notificationData: any): void {
    const notification = new Notification(notificationData);
    const lastActive = findLastIndex(this.notifications, 'isActive');
    this.notifications.splice(lastActive + 1, 0, notification);
  }

  private clearNotifications(needClear) {
    if (needClear) {
      this.notifications = [];
    }
  }
}
