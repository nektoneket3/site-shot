import { Injectable } from '@angular/core';

import { NotificationsStore } from '../store/notifications.store';


@Injectable()
export class NotifierService {

  constructor(private notificationsStore: NotificationsStore) {}

  success(message: string): void {
    this.notify({ message, color: 'green' });
  }

  error(message: string): void {
    this.notify({ message, color: 'red' });
  }

  warn(message: string): void {
    this.notify({ message, color: 'yellow' });
  }

  notify(notification: { message: string, color: string }): void {
    this.notificationsStore.add(notification);
  }
}
