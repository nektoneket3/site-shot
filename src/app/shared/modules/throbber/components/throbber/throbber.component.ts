import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';


type SmallSize = 'sm';

@Component({
  selector: 'app-throbber',
  templateUrl: './throbber.component.html',
  styleUrls: ['../../../../../../assets/styles/throbber.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ThrobberComponent implements OnInit {

  @Input() size: SmallSize;
  dots = new Array(8);

  ngOnInit() {}
}
