import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// components
import { ThrobberComponent } from './components/throbber/throbber.component';


@NgModule({
  declarations: [
    ThrobberComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ThrobberComponent,
  ]
})
export class ThrobberModule { }
