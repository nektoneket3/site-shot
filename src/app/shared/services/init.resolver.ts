import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { DefaultState } from 'shared/store/base.store';


export interface Init {
  init(initialData?: any): Promise<any>;
}

@Injectable()
export abstract class InitResolver implements Resolve<Promise<void | any>>, Init {

  protected constructor(protected store: DefaultState) {}

  resolve(): Promise<void> {
    return this.init();
  }

  async init(initialData?: any): Promise<void> {
    this.store.setErrors();
    this.store.init(initialData);
  }
}
