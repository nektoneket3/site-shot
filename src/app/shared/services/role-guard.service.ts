import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import { AuthStore } from 'shared/store/auth.store';


@Injectable({ providedIn: 'root' })
export class RoleGuardService implements CanActivate {

  constructor(
    private authStore: AuthStore,
    private router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const permittedRoles: string[] = route.data.permittedRoles;
    const notPermittedRoles: string[] = route.data.notPermittedRoles;
    const redirectTo: string[] = [route.data.redirectTo || '/login']; // TODO create something for route string

    if (permittedRoles) {
      if (!permittedRoles.includes(this.authStore.user.role)) {
        this.router.navigate(redirectTo);
        return false;
      }
    } else if (notPermittedRoles) {
      if (notPermittedRoles.includes(this.authStore.user.role)) {
        this.router.navigate(redirectTo);
        return false;
      }
    }

    return true;
  }
}
