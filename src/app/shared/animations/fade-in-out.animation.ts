import { animate, style, transition, trigger } from '@angular/animations';


export function fadeInOut(timing = 200, name = 'fadeInOut') {
  return trigger(name, [
    transition(':enter', [
      style({ opacity: 0 }),
      animate(timing, style({ opacity: 1 }))
    ]),
    transition(':leave', [
      animate(timing, style({ opacity: 0 }))
    ])
  ]);
}
