import { NgModule } from '@angular/core';
// modules
import { SharedModule } from 'shared/shared.module';
// directives
import { NavbarToggleDirective } from './directives/navbar-toggle.directive';
// components
import { HeaderComponent } from './components/header/header.component';
import { GuestNavigationComponent } from './components/navigation/guest/guest-navigation.component';
import { CustomerNavigationComponent } from './components/navigation/customer/customer-navigation.component';
import { NavItemComponent } from './components/navigation/nav-item/nav-item.component';


@NgModule({
  declarations: [
    NavbarToggleDirective,
    HeaderComponent,
    GuestNavigationComponent,
    CustomerNavigationComponent,
    NavItemComponent,
  ],
  imports: [
    SharedModule,
  ],
  exports: [
    HeaderComponent
  ]
})
export class LayoutModule { }
