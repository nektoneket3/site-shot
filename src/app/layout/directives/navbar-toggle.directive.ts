import { Directive, HostListener, Input, AfterViewInit } from '@angular/core';


@Directive({
  selector: '[appNavbarToggle]'
})
export class NavbarToggleDirective implements AfterViewInit {

  @Input('appNavbarToggle') id: string;

  appNav;
  toggleClass = 'show';

  ngAfterViewInit() {
    this.appNav = document.getElementById(this.id);
  }

  @HostListener('click', ['$event'])
  iconClick(event): void {
    this.appNav.classList.toggle(this.toggleClass);
    event.stopPropagation();
  }

  @HostListener('document:click', ['$event'])
  outsideClick(event): void {
    if (this.appNav.classList.contains(this.toggleClass) && !this.appNav.contains(event.target)) {
      this.appNav.classList.remove(this.toggleClass);
    }
  }
}
