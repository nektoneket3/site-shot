import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { computed } from 'mobx-angular';

import { AuthStore } from 'shared/store/auth.store';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  @computed get user() {
    return this.authStore.user;
  }

  @computed get userSettings() {
    return this.user.settings;
  }

  constructor(public authStore: AuthStore) {}

  ngOnInit() {}
}
