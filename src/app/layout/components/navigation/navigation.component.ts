import { Input } from '@angular/core';

import { User } from 'auth/models/user.model';


export class NavigationComponent {
  @Input() user: User;
}
