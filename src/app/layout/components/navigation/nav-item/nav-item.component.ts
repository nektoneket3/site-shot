import { Component, OnInit, Input } from '@angular/core';


export interface NavigationItem {
  route: string;
  name: string;
}

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss']
})
export class NavItemComponent implements OnInit {

  @Input() item: NavigationItem;

  ngOnInit() {}
}
