import { Component, OnInit } from '@angular/core';

import { NavigationComponent } from '../navigation.component';
import { NavigationItem } from '../nav-item/nav-item.component';


@Component({
  selector: 'app-guest-navigation',
  templateUrl: './guest-navigation.component.html',
  styleUrls: ['../navigation.component.scss']
})
export class GuestNavigationComponent extends NavigationComponent implements OnInit {

  // guestAvatar = '/assets/images/anonymous.png';

  navItems: NavigationItem[] = [
    { route: '/', name: 'Dashboard' },
    { route: '/login', name: 'Login' },
    { route: '/signup', name: 'Signup' }
  ];

  ngOnInit() {}
}
