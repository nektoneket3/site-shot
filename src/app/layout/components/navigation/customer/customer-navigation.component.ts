import { Component, OnInit } from '@angular/core';

import { NavigationComponent } from '../navigation.component';
import { NavigationItem } from '../nav-item/nav-item.component';
import { AuthService } from 'auth/services/auth.service';


@Component({
  selector: 'app-customer-navigation',
  templateUrl: './customer-navigation.component.html',
  styleUrls: ['../navigation.component.scss', './customer-navigation.component.scss']
})
export class CustomerNavigationComponent extends NavigationComponent implements OnInit {

  navItems: NavigationItem[] = [
    { route: '/', name: 'Dashboard' },
    { route: '/profile', name: 'Profile' },
  ];

  constructor(private authService: AuthService) {
    super();
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {}
}
